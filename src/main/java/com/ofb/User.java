package com.ofb;

import java.util.HashMap;

public class User {
	private Long userId;
	private HashMap<Product, Subscription> userSubscription = new HashMap<Product, Subscription>();

	public User(long userId) {
		this.userId=userId;
	}

	public HashMap<Product, Subscription> getUserSubscription() {
		return userSubscription;
	}

	public void setUserSubscription(
			HashMap<Product, Subscription> userSubscription) {
		this.userSubscription = userSubscription;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	//Depends only on userId
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Long.valueOf(userId).hashCode();  
        return result;
    }
 
    //Compare only userId
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (userId != other.userId)
            return false;
        return true;
    }
	
}
