package com.ofb;

import java.util.ArrayList;
import java.util.List;

public class ProductSubscription {
  private Long productId;
  private List<User> subscribedUsers = new ArrayList<User>();
  
public Long getProductId() {
	return productId;
}
public void setProductId(Long productId) {
	this.productId = productId;
}
public List<User> getSubscribedUsers() {
	return subscribedUsers;
}
public void setSubscribedUsers(List<User> subscribedUsers) {
	this.subscribedUsers = subscribedUsers;
}
}
