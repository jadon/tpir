package com.ofb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@Autowired
	private TPIRService service;

	@RequestMapping(value = "/subscribe",method=RequestMethod.POST)
    public String subscribe(@RequestBody SubscribeRequest subscribeRequest) {
	   return service.addSubscription(subscribeRequest);
    }
	
	@RequestMapping(value = "/unsubscribe",method=RequestMethod.POST)
    public String unsubscribe(@RequestBody UnsubscribeRequest unsubscribeRequest) {
       return service.unsubscribe(unsubscribeRequest);
    }
	
	@RequestMapping(value = "/priceDataPoint",method=RequestMethod.POST)
    public String updatePriceData(@RequestBody PriceDataPoint priceDataPoint) {
       return service.updatePrice(priceDataPoint);
    }
	
	@RequestMapping(value = "/getUserNotification/{userId}",method=RequestMethod.POST)
    public HashSet<Notification> updatePriceData(@PathVariable Long userId) {
       return service.readNotification(userId);
    }
	
	@RequestMapping(value = "/getAllUsers",method=RequestMethod.POST)
    public HashMap<Long,User> getAllUsers() {
       return service.getAllUsers();
    }
	
	@RequestMapping(value = "/getAllProducts",method=RequestMethod.POST)
    public HashMap<Long,Product> getAllProducts() {
       return service.getAllProdcuts();
    }
	
	@RequestMapping(value = "/getAllSubscribers/{prodId}/",method=RequestMethod.POST)
    public HashSet<User> getAllSubscribers(@PathVariable Long prodId) {
       return service.getAllSubscribers(prodId);
    }
}
