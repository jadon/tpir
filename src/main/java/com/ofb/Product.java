package com.ofb;


public class Product {

	public Product(Long productId, Long price, Long allTimeLowPrice, String url) {
		super();
		this.productId = productId;
		this.price = price;
		this.allTimeLowPrice = allTimeLowPrice;
		this.url = url;
	}
	private Long productId;
	private Long price;
	private Long allTimeLowPrice;
	private String url;
	
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getAllTimeLowPrice() {
		return allTimeLowPrice;
	}
	public void setAllTimeLowPrice(Long allTimeLowPrice) {
		this.allTimeLowPrice = allTimeLowPrice;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	//Depends only on productId
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Long.valueOf(productId).hashCode();  
        return result;
    }
 
    //Compare only productId
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (productId != other.productId)
            return false;
        return true;
    }
	
}
