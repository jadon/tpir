package com.ofb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.ofb.Notification.Message;

@Service
public class TPIRService {
	private final static HashMap<Long, User> users = new HashMap<Long, User>();
	private final static HashMap<Long, Product> products = new HashMap<Long, Product>();
	private static ConcurrentHashMap<Product, HashSet<User>> productSubscription = new ConcurrentHashMap<Product, HashSet<User>>();
	private static ConcurrentHashMap<Long, HashSet<Notification>> notifications = new ConcurrentHashMap<Long, HashSet<Notification>>();

	static { 
		for (int i = 0; i < 20; i++) {
			long price = 10000;
			User user = new User((long) i);
			users.put((long) i, user);
			Product product = new Product((long) i, price, price,
					"amazon.com/" + i);
			products.put((long) i, product);
		}
	}

	public HashMap<Long, Product> getAllProdcuts() {
		return products;
	}

	public HashMap<Long, User> getAllUsers() {
		return users;
	}

	public HashSet<User> getAllSubscribers(Long prodId) {
		return productSubscription.get(products.get(prodId));
	}

	public String unsubscribe(UnsubscribeRequest usr) {
		User user = users.get(usr.getUserId());
		List<Subscription> subscriptions = usr.getUnsubscribe();
		HashSet<User> userSub = new HashSet<User>();
		if (null != user) {
			for (Subscription subscription : subscriptions) {
				Product product = products.get(subscription.getProductId());
				if (productSubscription.get(product) != null) {
					userSub = productSubscription.get(product);
					if (userSub != null) {
						userSub.remove(user);
						user.getUserSubscription().remove(product);
					}
				}
			}
			return "Success";
		} else
			return "User Not Found";
	}

	public String updatePrice(PriceDataPoint pdp) {
		boolean allTimeLow = false;
		Product product = products.get(pdp.getProductId());
		if(product!=null){
		Long currentPrice = product.getPrice();
		product.setPrice(pdp.getPrice());
		product.setUrl(pdp.getUrl());
		if (pdp.getPrice() < product.getAllTimeLowPrice()) {
			product.setAllTimeLowPrice(pdp.getPrice());
			allTimeLow = true;
		}
		createNotification(pdp.getPrice(), pdp.getProductId(), currentPrice,
				pdp.getPrice(), allTimeLow);
		return "success";
		}
		else{
		return "Product Not Found";
		}
	}

	public void createNotification(Long userId, Long changedProductId,
			Long currentPrice, Long changedPrice, boolean allTimeLow) {
		// Get all users subscribed to this changed product
		HashSet<User> usersSub = productSubscription.get(products.get(changedProductId));
		if (currentPrice > changedPrice) {
				// Notify users subscribed to less_than_10

				for (User userSub : usersSub) {
					Notification notification = new Notification();
					if ((currentPrice - changedPrice) / currentPrice * 100 < 10 && userSub.getUserSubscription().get(products.get(changedProductId)).getWhen().equals(Message.MORE_THAN_10.toString())) {
						notification
								.setMessage(Notification.Message.MORE_THAN_10
										.toString());
						notification.setPrice(changedPrice);
						notification.setProductId(changedProductId);
						notification.setRead(false);
				}
					else if (allTimeLow && userSub.getUserSubscription().get(products.get(changedProductId)).getWhen().equals(Message.ALL_TIME_LOW.toString())) {
					notification
							.setMessage(Notification.Message.ALL_TIME_LOW
									.toString());
					notification.setPrice(changedPrice);
					notification.setProductId(changedProductId);
					notification.setRead(false);
				}
					else{
						notification.setMessage(Notification.Message.ALWAYS
								.toString());
						notification.setPrice(changedPrice);
						notification.setProductId(changedProductId);
						notification.setRead(false);
					}
					if (notifications.get(userSub.getUserId()) != null) {
						HashSet<Notification> notifyList = notifications.get(userSub.getUserId());
						  notifyList.add(notification);
						  notifications.put(userSub.getUserId(), notifyList);
					}
					else{
						HashSet<Notification> notifyList = new HashSet<Notification>();
						if(!notifyList.add(notification)) {
							notifyList.remove(notification);
							notifyList.add(notification);
						 }
						;
						 notifications.put(userSub.getUserId(), notifyList);
					}
			}
	}
	}

	public HashSet<Notification> readNotification(Long userId) {
		return notifications.get(userId);
	}

	public String addSubscription(SubscribeRequest sr) {
		User user = users.get(sr.getUserId());
		if (null != user) {
			List<Subscription> subscriptions = sr.getSubscribe();
			for (Subscription subscription : subscriptions) {
				Product product = products.get(subscription.getProductId());
				user.getUserSubscription().put(product, subscription);
				if (productSubscription.get(product) == null) {
					HashSet<User> userSub = new HashSet<User>();
					userSub.add(users.get(sr.getUserId()));
					productSubscription.put(
							products.get(subscription.getProductId()), userSub);
				} else {
					HashSet<User> userSub = productSubscription.get(products.get(subscription
							.getProductId()));
					userSub.add(users.get(sr.getUserId()));
					productSubscription.put(
							products.get(subscription.getProductId()), userSub);
				}
			}
			return "success";
		}
		return "user not found";
	}
}