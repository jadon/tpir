package com.ofb;

import java.util.ArrayList;
import java.util.List;

public class UnsubscribeRequest {
	private Long userId;
	private List<Subscription> unsubscribe = new ArrayList<Subscription>();
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public List<Subscription> getUnsubscribe() {
		return unsubscribe;
	}
	public void setUnsubscribe(List<Subscription> unsubscribe) {
		this.unsubscribe = unsubscribe;
	}
}
