package com.ofb;

public class Notification {
  private Long productId;
  private String url;
  private Long price;
  private String message;
  private Boolean read;
  
public Long getProductId() {
	return productId;
}
public void setProductId(Long productId) {
	this.productId = productId;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public Long getPrice() {
	return price;
}
public void setPrice(Long price) {
	this.price = price;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public Boolean getRead() {
	return read;
}
public void setRead(Boolean read) {
	this.read = read;
}

public static enum Message{
	MORE_THAN_10,ALL_TIME_LOW,ALWAYS;
}

//Depends only on productId and UserId
@Override
public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Long.valueOf(productId).hashCode();  
    return result;
}

//Compare only productId and UserId, so that a user will get just one Notification for a particular product type
@Override
public boolean equals(Object obj) {
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;
    Notification other = (Notification) obj;
    if (productId != other.productId)
        return false;
    return true;
}
}
