package com.ofb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfbApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfbApplication.class, args);
	}
}
