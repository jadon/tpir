package com.ofb;

public class Subscription {
	public Subscription() {
	}

	public Subscription(Long productId, Long subscriptionPrice,
			Long lastSentPrice, String subType) {
		super();
		this.productId = productId;
		this.subscriptionPrice = subscriptionPrice;
//		this.lastSentPrice = lastSentPrice;
		this.setWhen(subType);
	}

	private Long productId;
	private Long subscriptionPrice;
	private Long lastSentPrice;
	private String when;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getSubscriptionPrice() {
		return subscriptionPrice;
	}

	public void setSubscriptionPrice(Long subscriptionPrice) {
		this.subscriptionPrice = subscriptionPrice;
	}

	public Long getLastSentPrice() {
		return lastSentPrice;
	}

	public void setLastSentPrice(Long lastSentPrice) {
		this.lastSentPrice = lastSentPrice;
	}

	public String getWhen() {
		return when;
	}

	public void setWhen(String when) {
		this.when = when;
	}
}
