package com.ofb;

import java.util.ArrayList;
import java.util.List;

public class SubscribeRequest {
	private Long userId;
	private List<Subscription> subscribe = new ArrayList<Subscription>();

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Subscription> getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(List<Subscription> subscribe) {
		this.subscribe = subscribe;
	}
}
